import type { NextPage } from 'next'
import Head from 'next/head'
import {SearchIcon} from "@heroicons/react/outline"
import {ArrowLeftIcon} from "@heroicons/react/solid"
import logo from '/icons/logo.svg'
import Image from "next/image";
import MyDropdown from "../components/DropDown";
import Footer from "../components/Footer";
import {useRef} from "react";
import {useRouter} from "next/router";



const Home: NextPage = () => {
    const router:any=useRouter();
    const searchInputRef:any = useRef();
    const search = (e:any)=>{
            e.preventDefault();
            const term:string=searchInputRef.current.value
            if (!term) return;

            router.push(`/search?pkg=${term}`);
    }
    return (
        <div className='flex flex-col items-center justify-center h-screen'>
            <Head>
                <title>rayman.</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>

            {/*Header*/}
            <header className='flex w-full p-5 items-start justify-end absolute left-0 top-0 '>
                <div className='flex space-x-4'>
                    <MyDropdown></MyDropdown>
                </div>
            </header>

            {/*Body*/}
            <form className='flex flex-col items-center mt-32 flex-grow w-full'>
                <div className='max-w-xs sm:max-w-xl'><Image src={logo}  loading={"lazy"}></Image></div>
                <div className="flex focus-within:shadow-md w-full max-w-sm rounded-full border border-gray-200 px-5 py-3 items-center sm:max-w-xl lg:max-w-2xl" >
                    <SearchIcon className='h-5 mr-3 text-gray-500'/>
                    <input ref={searchInputRef} className='flex-grow focus:outline-none' />
                    <button onClick={search} className='flex rounded-full p-1 hover:bg-gray-100' type='submit'><ArrowLeftIcon className='h-5 text-orange-500' /></button>
                </div>
            </form>

            {/*Footer*/}
            <Footer/>
        </div>
    )
}

export default Home
