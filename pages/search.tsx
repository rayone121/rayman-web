import React from 'react';
import Head from "next/head";
import Header from "../components/Header";
import SearchResults from "../components/searchresults";
import {Router} from "next/router";
import DumyData from "../DumyData";



function Search({results}:any) {
    console.log(results);
    return (
        <div>
            <Head>
                <title> rayman - search </title>
                <link rel='icon' href='/favicon.ico'/>
            </Head>
            {/*Header*/}
            <Header/>

            {/*Serach Results*/}
            <SearchResults data={results}/>
        </div>
    );
}

export default Search;

export async function getServerSideProps(context:Router){
    const useDummyData = true;

    const data = useDummyData ? DumyData : await fetch(
        `https://aur.archlinux.org/rpc?v=5&type=search&arg=${context.query.pkg}`
    ).then((response) => response.json())

    return{
        props:{
            results: data,
        },
    };
}