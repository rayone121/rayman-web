import React, {Ref, useRef} from 'react';
import logo from '/icons/logo1.svg'
import Image from "next/image";
import {useRouter} from "next/router";
import {SearchIcon, TerminalIcon} from "@heroicons/react/outline";
import {PlayIcon,XIcon} from "@heroicons/react/solid";



function Header() {
    const searchInputRef:Ref<any> = useRef(null);
    const router = useRouter();
    const search = (e:any) => {
        e.preventDefault();
        const term:string=searchInputRef.current.value
        if (!term) return;

        router.push(`/search?pkg=${term}`);

    }
    return (
        <header className='sticky top-0 bg-white'>
          <div className='flex w-full p-6 items-center'>
          <Image
              src={logo}
              height={80}
              width={150}
              loading={"lazy"}
              onClick={() => router.push('/')}
          >
          </Image>
            <form className='flex flex-grow border border-gray-200 rounded-full shadow-md max-w-3xl items-center px-6 py-3 ml-10 mr-5'>
                <input ref={searchInputRef} className='flex-grow w-full focus:outline-none' type="text" />
                <XIcon className='h-7 text-gray-700 transition duration-100 transform hover:scale-125 mr-3 ' onClick={() => searchInputRef.current.value = ""}/>
                <button onClick={search} type='submit' className='hidden md:flex'><SearchIcon className='inline-flex mr-3 h-6 text-orange-400 border-l-2 pl-4 border-gray-300 hover:text-orange-500  '/></button>
            </form>
              <div className='flex space-x-4 ml-auto'>
                  <button className='hidden md:flex shadow text-gray-700 rounded-full border-gray-100 p-3 border hover:bg-gray-200 items-center '><TerminalIcon className='h-6' /></button>
                  <button className='hidden md:flex shadow text-gray-700 rounded-full border-gray-100 p-3 border hover:bg-gray-200 items-center '><PlayIcon className='h-6' /><p className='hidden lg:flex'>Run Operations</p></button>
              </div>

          </div>
        </header>
    );
}

export default Header;