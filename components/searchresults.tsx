import React from 'react';
import {DownloadIcon} from "@heroicons/react/outline";

function SearchResults({data}:any) {
    return (
        <div className='mx-auto w-full px-3 sm:pl-[5%] md:pl[15%] lg:pl-52'>
            <p className='text-gray-700 text-md mb-5 mt-3'>{data.resultcount} results</p>
            {data.results?.map((result:any) => (
                <div key={result.URLPath} className='flex max-h-32 p-5 my-4 max-w-4xl rounded-full text-gray-700 border-gray-300 border'>
                    <div className='flex-col flex-grow justify-center'>
                        <a className='flex px-5' href={`https://aur.archlinux.org/packages/${result.Name}`}>{result.Name}</a>
                        <p className='flex px-5 max-w-2xl text-sm '>{result.Description}</p>
                    </div>
                    <div className='flex justify-end'>
                        <DownloadIcon className='h-10'></DownloadIcon>
                    </div>

                </div>
            ))}
        </div>
    );
}

export default SearchResults;