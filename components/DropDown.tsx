import { Fragment } from 'react'
import { Menu, Transition } from '@headlessui/react'
import {ViewGridIcon} from '@heroicons/react/solid'
import {ArrowCircleDownIcon, CogIcon, RefreshIcon} from "@heroicons/react/outline";

function classNames(...classes:any) {
    return classes.filter(Boolean).join(' ')
}

function MyDropdown() {
    return (
        <Menu as="div" className="relative inline-block text-left">
            <div>
                <Menu.Button className="inline-flex justify-center rounded-full border border-gray-300 shadow-sm bg-white text-sm font-medium hover:bg-gray-100 ">
                    <ViewGridIcon className='h-10 w-10 p-2 text-gray-700'></ViewGridIcon>
                </Menu.Button>
            </div>

            <Transition
                as={Fragment}
                enter="transition ease-out duration-100"
                enterFrom="transform opacity-0 scale-95"
                enterTo="transform opacity-100 scale-100"
                leave="transition ease-in duration-75"
                leaveFrom="transform opacity-100 scale-100"
                leaveTo="transform opacity-0 scale-95"
            >
                <Menu.Items className="origin-top-right absolute right-0 mt-2 w-50 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none">
                    <div className="py-1">
                        <Menu.Item>
                            {({ active }) => (
                                <a
                                    href="#"
                                    className={classNames(
                                        active ? 'flex bg-gray-100 text-gray-900 ' : 'flex text-gray-700',
                                        'block px-4 py-2 text-sm'
                                    )}
                                >
                                    <ArrowCircleDownIcon className='flex h-6 w-6 pr-2'/>Installed
                                </a>
                            )}
                        </Menu.Item>
                        <Menu.Item>
                            {({ active }) => (
                                <a
                                    href="#"
                                    className={classNames(
                                        active ? 'flex bg-gray-100 text-gray-900' : 'flex text-gray-700',
                                        'block px-4 py-2 text-sm'
                                    )}
                                >
                                    <RefreshIcon className='flex h-6 w-6 pr-2'/>Updates
                                </a>
                            )}
                        </Menu.Item>
                        <Menu.Item>
                            {({ active }) => (
                                <a
                                    href="#"
                                    className={classNames(
                                        active ? 'flex bg-gray-100 text-gray-900' : 'flex text-gray-700',
                                        'block px-4 py-2 text-sm'
                                    )}
                                >
                                    <CogIcon className='flex h-6 w-6 pr-2' />Settings
                                </a>
                            )}
                        </Menu.Item>
                    </div>
                </Menu.Items>
            </Transition>
        </Menu>
    )
}

export default MyDropdown;